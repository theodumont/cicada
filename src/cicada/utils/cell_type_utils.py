import yaml

def read_cell_type_categories_yaml_file(yaml_file, using_multi_class=2):
    """
    Read cell type categories from a yaml file. If more than 2 type cells are given, then a multi-class
    classifier will be used. If 2 type cells are given, then either it could be multi-class or binary classifier,
    then this choice should be given in the parameters of CinacModel.
    If 2 cell-type are given, for binary classifier, it should be precised which cell type should be predicted
    if we get more than 0.5 probability.
    Args:
        yaml_file:
        using_multi_class: int, give the default number of classes used, not necessary if already
        put in the yaml file

    Returns: cell_type_from_code_dict, cell_type_to_code_dict, multi_class_arg
    cell_type_from_code_dict: dict, key is an int, value is the cell_type
    cell_type_to_code_dict: dict, key is a string, value is the code of the cell_type. A code can have more than one
    string associated, but all of them represent the same cell_type defined in cell_type_from_code_dict
    multi_class_arg: is None if no multi_class_arg was given in the yaml_file, True or False, if False means we want
    to use a binary classifier

    """

    cell_type_from_code_dict = dict()
    cell_type_to_code_dict = dict()

    with open(yaml_file, 'r') as stream:
        yaml_data = yaml.load(stream, Loader=yaml.FullLoader)

    multi_class_arg = None
    n_cell_categories = 0
    category_code_increment = 0
    # sys.stderr.write(f"{analysis_args_from_yaml}")
    for arg_name, args_content in yaml_data.items():
        if arg_name == "config":
            if isinstance(args_content, dict):
                if "multi_class" in args_content:
                    multi_class_arg = bool(args_content["multi_class"])

        if arg_name == "cell_type_categories":
            n_cell_categories = len(args_content)
            # if multi_class == 1, then we need a predicted_cell_type which will have the value 1
            predicted_cell_type = None
            print(f"args_content {args_content}")
            # args_content is a list of dict, key is the cell_type name, value is a dict
            # with keywords (list of string as value) and predicted_celll_type (bool as value)
            if (multi_class_arg is None and using_multi_class == 1) or (multi_class_arg is False):
                # for cell_type_category, category_dict in args_content.items():
                #     if "predicted_celll_type" in category_dict:
                #         predicted_cell_type = cell_type_category
                for cell_type_dict in args_content:
                    for cell_type_category, category_dict in cell_type_dict.items():
                        if "predicted_celll_type" in category_dict:
                            predicted_cell_type = cell_type_category

            # for cell_type_category, category_dict in args_content.items():
            #     if (predicted_cell_type is not None) and (predicted_cell_type == cell_type_category) and \
            #             (n_cell_categories <= 2):
            #         cell_type_from_code_dict[1] = cell_type_category
            #         cell_type_code = 1
            #     else:
            #         cell_type_from_code_dict[category_code_increment] = cell_type_category
            #         cell_type_code = category_code_increment
            #         print(f"cell_type_category {cell_type_category}: {cell_type_code}")
            #         category_code_increment += 1
            #         if (category_code_increment == 1) and (predicted_cell_type is not None) and \
            #                 (n_cell_categories <= 2):
            #             category_code_increment += 1
            #     cell_type_to_code_dict[cell_type_category] = cell_type_code
            #     if "keywords" in category_dict:
            #         for keyword in category_dict["keywords"]:
            #             cell_type_to_code_dict[keyword] = cell_type_code
            for cell_type_dict in args_content:
                for cell_type_category, category_dict in cell_type_dict.items():
                    if (predicted_cell_type is not None) and (predicted_cell_type == cell_type_category) and \
                            (n_cell_categories <= 2):
                        cell_type_from_code_dict[1] = cell_type_category
                        cell_type_code = 1
                    else:
                        cell_type_from_code_dict[category_code_increment] = cell_type_category
                        cell_type_code = category_code_increment
                        print(f"cell_type_category {cell_type_category}: {cell_type_code}")
                        category_code_increment += 1
                        if (category_code_increment == 1) and (predicted_cell_type is not None) and \
                                (n_cell_categories <= 2):
                            category_code_increment += 1
                    cell_type_to_code_dict[cell_type_category] = cell_type_code
                    if "keywords" in category_dict:
                        for keyword in category_dict["keywords"]:
                            cell_type_to_code_dict[keyword] = cell_type_code

    return cell_type_from_code_dict, cell_type_to_code_dict, multi_class_arg
