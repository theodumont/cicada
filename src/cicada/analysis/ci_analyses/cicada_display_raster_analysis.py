from cicada.analysis.cicada_analysis import CicadaAnalysis
from time import time
from cicada.utils.display.rasters import plot_raster
import numpy as np
# from cicada.utils.display.colors import rgb_to_name
from cicada.utils.misc import from_timestamps_to_frame_epochs, from_timestamps_array_to_list, \
    validate_indices_in_string_format, \
    extract_indices_from_string


class CicadaDisplayRasterAnalysis(CicadaAnalysis):
    def __init__(self, config_handler=None):
        """
        """
        long_description = '<p align="center"><b>Display neuronal data</b></p><br>'
        long_description = long_description + 'Save plots of neuronal data as a cells by times raster of discrete ' \
                                              '(like spikes) or continuous data (like fluorescence signal).<br><br>'
        long_description = long_description + 'It is possible to specify epochs that will be colored accordingly.'
        CicadaAnalysis.__init__(self, name="Raster", family_id="Display",
                                short_description="Display raster",
                                long_description=long_description,
                                config_handler=config_handler,
                                accepted_data_formats=["CI_DATA"])

        # key is the cell type name, value is the name of the arg to get the color from the widget
        self.cell_type_color_arg_name = dict()

    def copy(self):
        """
        Make a copy of the analysis
        Returns:

        """
        analysis_copy = CicadaDisplayRasterAnalysis(config_handler=self.config_handler)
        self.transfer_attributes_to_tabula_rasa_copy(analysis_copy=analysis_copy)
        return analysis_copy

    def check_data(self):
        """
        Check the data given one initiating the class and return True if the data given allows the analysis
        implemented, False otherwise.
        :return: a boolean
        """
        super().check_data()

        for session_index, session_data in enumerate(self._data_to_analyse):
            if session_data.DATA_FORMAT != "CI_DATA":
                self.invalid_data_help = f"Non CI_DATA format compatibility not yet implemented: " \
                                         f"{session_data.DATA_FORMAT}"
                return False

        for data_to_analyse in self._data_to_analyse:
            roi_response_series = data_to_analyse.get_roi_response_series()
            if len(roi_response_series) == 0:
                self.invalid_data_help = f"No roi response series available in " \
                    f"{data_to_analyse.identifier}"
                return False

        return True

    def set_arguments_for_gui(self):
        """

        Returns:

        """
        CicadaAnalysis.set_arguments_for_gui(self)

        self.add_roi_response_series_arg_for_gui(short_description="Neural activity to use", long_description=None)

        # choices_dict = dict()
        # for index, data in enumerate(self._data_to_analyse):
        #     if index < 2:
        #         choices_dict[data.identifier] = ["test_1", "test_2"]
        #     else:
        #         choices_dict[data.identifier] = ["test_3", "test_4", "test_6"]
        #
        # test_arg = {"arg_name": "test_multiple_choices", "choices": choices_dict,
        #             "short_description": "Test choices for each session",
        #             "multiple_choices": True}
        #
        # self.add_argument_for_gui(**test_arg)

        all_intervals = []
        for data_to_analyse in self._data_to_analyse:
            all_intervals.extend(data_to_analyse.get_intervals_names())
            all_intervals.extend(data_to_analyse.get_behavioral_epochs_names())
        all_intervals = list(np.unique(all_intervals))
        # print(f"all_intervals {all_intervals}")

        if len(all_intervals) > 0:
            self.add_choices_for_groups_for_gui(arg_name="interval_names", choices=all_intervals,
                                                with_color=True,
                                                mandatory=False,
                                                short_description="Epochs",
                                                long_description="Select epochs you want to color in the raster",
                                                family_widget="epochs")

            self.add_int_values_arg_for_gui(arg_name="intervals_alpha_color", min_value=1, max_value=100,
                                            short_description="Transparency of color bands",
                                            default_value=50, family_widget="epochs")

            self.add_bool_option_for_gui(arg_name="span_area_only_on_raster", true_by_default=False,
                                         short_description="Span only on raster",
                                         long_description="If checked, means the span will also be displayed in "
                                                          "the sum of activity plot if existing",
                                         family_widget="epochs")

        all_cell_types = []
        for data_to_analyse in self._data_to_analyse:
            all_cell_types.extend(data_to_analyse.get_all_cell_types())

        all_cell_types = list(set(all_cell_types))

        self.add_choices_for_groups_for_gui(arg_name="cells_groups", choices=all_cell_types,
                                            with_color=True,
                                            mandatory=False,
                                            short_description="Groups of cells",
                                            long_description="Select cells's groups you want to color in the raster",
                                            family_widget="cell_type",
                                            add_custom_group_field=True,
                                            custom_group_validation_fct=validate_indices_in_string_format,
                                            custom_group_processor_fct=None)
        self.add_bool_option_for_gui(arg_name="sort_raster_by_cell_type", true_by_default=False,
                                     short_description="Sort raster cells' groups ",
                                     family_widget="cell_type")

        spike_shapes = ["|", "o", ".", "*"]
        self.add_choices_arg_for_gui(arg_name="spike_shape", choices=spike_shapes,
                                     default_value="|", short_description="Spikes shape",
                                     multiple_choices=False,
                                     family_widget="spikes")

        self.add_int_values_arg_for_gui(arg_name="spike_size", min_value=5, max_value=500,
                                        short_description="Spike size",
                                        default_value=5, long_description=None, family_widget="spikes")

        self.add_color_arg_for_gui(arg_name="spikes_color", default_value=(1, 1, 1, 1.),
                                   short_description="spikes color",
                                   long_description=None, family_widget="spikes")

        self.add_int_values_arg_for_gui(arg_name="traces_lw", min_value=1, max_value=500,
                                        short_description="Traces line width",
                                        default_value=30, family_widget="traces")

        self.add_bool_option_for_gui(arg_name="use_brewer_colors_for_traces", true_by_default=True,
                                     short_description="Use brewer colors for traces",
                                     long_description="If False, will use spectral color map",
                                     family_widget="traces")

        self.add_bool_option_for_gui(arg_name="display_dashed_line_with_traces", true_by_default=False,
                                     short_description="Dashlines with traces",
                                     long_description="Display a dashline representing the mean value of each cell "
                                                      "fluoresence' signal",
                                     family_widget="traces")

        self.add_color_arg_for_gui(arg_name="background_color", default_value=(0, 0, 0, 1.),
                                   short_description="background color",
                                   long_description=None, family_widget="raster_config")

        self.add_color_arg_for_gui(arg_name="activity_sum_plot_color", default_value=(1, 1, 1, 1.),
                                   short_description="Activity sum plot color",
                                   long_description=None, family_widget="raster_config")

        self.add_color_arg_for_gui(arg_name="y_ticks_labels_color", default_value=(1, 1, 1, 1.),
                                   short_description="Y axis ticks labels color",
                                   long_description=None, family_widget="raster_config")

        self.add_color_arg_for_gui(arg_name="x_ticks_labels_color", default_value=(1, 1, 1, 1.),
                                   short_description="X axis ticks labels color",
                                   long_description=None, family_widget="raster_config")

        self.add_field_text_option_for_gui(arg_name="raster_y_axis_label", default_value="",
                                           short_description="Raster y axis label",
                                           long_description=None, family_widget="raster_config")

        self.add_int_values_arg_for_gui(arg_name="raster_y_axis_label_size", min_value=1, max_value=100,
                                        short_description="Raster y axis label size",
                                        default_value=10, family_widget="raster_config")

        self.add_color_arg_for_gui(arg_name="axes_label_color", default_value=(1, 1, 1, 1.),
                                   short_description="Axes label color",
                                   long_description=None, family_widget="raster_config")

        self.add_int_values_arg_for_gui(arg_name="x_axis_ticks_label_size", min_value=1, max_value=50,
                                        short_description="X axis ticks label size",
                                        default_value=8, family_widget="raster_config")

        self.add_int_values_arg_for_gui(arg_name="y_axis_ticks_label_size", min_value=1, max_value=50,
                                        short_description="Y axis ticks label size",
                                        default_value=8, family_widget="raster_config")

        self.add_int_values_arg_for_gui(arg_name="bin_size_ms_for_spikes_sum", min_value=10, max_value=500,
                                        short_description="Bin size in ms for activity sum",
                                        default_value=150, family_widget="raster_config")

        self.add_bool_option_for_gui(arg_name="hide_x_ticks_labels", true_by_default=False,
                                     short_description="Hide x ticks labels",
                                     family_widget="raster_config")

        self.add_bool_option_for_gui(arg_name="hide_raster_y_ticks_labels", true_by_default=True,
                                     short_description="Hide raster y ticks labels",
                                     family_widget="raster_config")

        self.add_bool_option_for_gui(arg_name="with_ticks", true_by_default=False,
                                     short_description="Ticks on plots",
                                     family_widget="raster_config")

        self.add_bool_option_for_gui(arg_name="with_activity_sum", true_by_default=True,
                                     short_description="With activity sum plot",
                                     long_description=None, family_widget="raster_config")

        self.add_image_format_package_for_gui()

        self.add_verbose_arg_for_gui()

        self.add_with_timestamp_in_filename_arg_for_gui()

    def update_original_data(self):
        """
        To be called if the data to analyse should be updated after the analysis has been run.
        :return: boolean: return True if the data has been modified
        """
        pass

    def run_analysis(self, **kwargs):
        """
        test
        :param kwargs:
          segmentation

        :return:
        """
        CicadaAnalysis.run_analysis(self, **kwargs)

        roi_response_series_dict = kwargs["roi_response_series"]

        verbose = kwargs.get("verbose", True)

        hide_x_ticks_labels = kwargs.get("hide_x_ticks_labels")

        hide_raster_y_ticks_labels = kwargs.get("hide_raster_y_ticks_labels")

        with_ticks = kwargs.get("with_ticks")

        spike_shape = kwargs.get("spike_shape", "|")

        spike_size = kwargs.get("spike_size", "5")
        spike_size = spike_size * 0.01

        use_brewer_colors_for_traces = kwargs.get("use_brewer_colors_for_traces", True)

        background_color = kwargs.get("background_color")

        spikes_color = kwargs.get("spikes_color")

        activity_sum_plot_color = kwargs.get("activity_sum_plot_color")

        axes_label_color = kwargs.get("axes_label_color")

        with_activity_sum = kwargs.get("with_activity_sum")

        display_dashed_line_with_traces = kwargs.get("display_dashed_line_with_traces")

        xkcd_mode = kwargs.get("xkcd_mode", False)

        # ------------- CELL TYPES ---------------
        sort_raster_by_cell_type = kwargs.get("sort_raster_by_cell_type", False)

        cells_groups_dict = kwargs.get("cells_groups")
        # ------------- CELL TYPES ---------------

        # ------------- EPOCHS ---------------
        interval_names = kwargs.get("interval_names")

        intervals_alpha_color = kwargs.get("intervals_alpha_color", 100)
        intervals_alpha_color = intervals_alpha_color * 0.01

        span_area_only_on_raster = kwargs.get("span_area_only_on_raster", False)
        # ------------- EPOCHS ---------------

        traces_lw = kwargs.get("traces_lw")
        traces_lw = traces_lw * 0.01

        bin_size_ms_for_spikes_sum = kwargs.get("bin_size_ms_for_spikes_sum")

        raster_y_axis_label = kwargs.get("raster_y_axis_label")
        if raster_y_axis_label.strip() == "":
            raster_y_axis_label = None
        raster_y_axis_label_size = kwargs.get("raster_y_axis_label_size")

        y_axis_ticks_label_size = kwargs.get("y_axis_ticks_label_size")
        x_axis_ticks_label_size = kwargs.get("x_axis_ticks_label_size")

        y_ticks_labels_color = kwargs.get("y_ticks_labels_color")
        x_ticks_labels_color = kwargs.get("x_ticks_labels_color")

        # image package format
        save_formats = kwargs["save_formats"]
        if save_formats is None:
            save_formats = "pdf"

        save_raster = True

        dpi = kwargs.get("dpi", 100)

        width_fig = kwargs.get("width_fig")

        height_fig = kwargs.get("height_fig")

        with_timestamps_in_file_name = kwargs.get("with_timestamp_in_file_name", True)

        n_sessions = len(self._data_to_analyse)

        for session_index, session_data in enumerate(self._data_to_analyse):
            session_identifier = session_data.identifier
            if verbose:
                print(f"-------------- {session_identifier} -------------- ")
            if isinstance(roi_response_series_dict, dict):
                roi_response_serie_info = roi_response_series_dict[session_identifier]
            else:
                roi_response_serie_info = roi_response_series_dict

            neuronal_data = session_data.get_roi_response_serie_data(keys=roi_response_serie_info)

            neuronal_data_timestamps = np.asarray(session_data.get_roi_response_serie_timestamps(keys=roi_response_serie_info))

            frame_times = None
            if len(neuronal_data_timestamps) > 2:
                # if the timestamps is 2? then it means it is not really timestamps, but just the frame index
                if neuronal_data_timestamps[2] != 2:
                    frame_times = neuronal_data_timestamps
                    # print(f"np.max(frame_times) {np.max(frame_times)}")

            # TODO: Plan to display raster with timestamps on x-axis instead, so ci_pause will appears
            # ci_ts = session_data.get_ci_movie_time_stamps()

            # ------------- EPOCHS ---------------
            span_area_coords = None
            span_area_colors = None
            if (interval_names is not None) and (len(interval_names) > 0):
                span_area_coords = []
                span_area_colors = []
                print(f"get_behavioral_epochs_names {session_data.get_behavioral_epochs_names()}")
                for interval_group_name, interval_info in interval_names.items():
                    if len(interval_info) != 2:
                        continue
                    interval_names_in_group = interval_info[0]
                    interval_color = interval_info[1]
                    # print(f"Interval {interval_group_name}, color: "
                    #       f"{rgb_to_name(interval_color, with_float_values=True)}")
                    rdb_values = [int(np.round(c*255)) for c in interval_color]
                    rdb_values = rdb_values[:-1]
                    print(f"Interval {interval_group_name}, color: {rdb_values}")

                    intervals_in_group = []
                    # TODO: See for fusioning epochs from a same group so there are extended
                    for interval_name in interval_names_in_group:
                        # looking in behavior or intervals
                        intervals_timestamps = session_data.get_interval_times(interval_name=interval_name)
                        if intervals_timestamps is None:
                            intervals_timestamps = session_data.get_behavioral_epochs_times(epoch_name=interval_name)
                        if intervals_timestamps is None:
                            # means this session doesn't have this epoch name
                            print(f"{interval_name}: intervals_timestamps is None")
                            continue

                        if frame_times is None:
                            # means we don't know the timestamps
                            continue

                        # putting it as a list format, listof tuple of float
                        intervals_timestamps = from_timestamps_array_to_list(time_stamps_array=intervals_timestamps,
                                                                             frames_timestamps=neuronal_data_timestamps)
                        intervals_in_group.extend(intervals_timestamps)
                        # # now we want to get the intervals time_stamps and convert them in frames
                        # intervals_frames = from_timestamps_to_frame_epochs(time_stamps_array=intervals_timestamps,
                        #                                                    frames_timestamps=neuronal_data_timestamps,
                        #                                                    as_list=True)
                        # intervals_in_group.extend(intervals_frames)
                    span_area_coords.append(intervals_in_group)
                    span_area_colors.append(interval_color)
            # ------------- END EPOCHS ---------------
            # print(f"timestamps[:5] {timestamps[:5]}")
            cells_to_highlight = None
            cells_to_highlight_colors = None

            cells_order = np.arange(len(neuronal_data))

            if (cells_groups_dict is not None) and (len(cells_groups_dict) > 0):
                cell_indices_by_cell_type = session_data.get_cell_indices_by_cell_type(roi_serie_keys=
                                                                                       roi_response_serie_info)
                cells_to_highlight = []
                cells_to_highlight_colors = []
                if sort_raster_by_cell_type:
                    # ordering the data by cell type
                    last_index = 0
                    cells_added = []
                    new_neuronal_data = np.zeros(neuronal_data.shape)
                # TODO: See to check if groups don't contain some of the cells
                #  so far if that's the case it won't crash but some cells will be displayed several times
                # for cell_type, cell_indices in cell_indices_by_cell_type.items():
                for cells_group_name, cells_group_info in cells_groups_dict.items():
                    if len(cells_group_info) != 2:
                        continue
                    cells_names_in_group = cells_group_info[0]
                    cells_group_color = cells_group_info[1]
                    for cells_name in cells_names_in_group:
                        # then we extract the cell_indices from the cells_group_name
                        # it is either a string representing the cell_type or a series of indices
                        if cells_name in cell_indices_by_cell_type:
                            cell_indices = cell_indices_by_cell_type[cells_name]
                        else:
                            cell_indices = np.array(extract_indices_from_string(cells_name))
                        print(f"N cells in {cells_name}: {len(cell_indices)}")
                        if len(cell_indices) == 0:
                            continue
                        # making sure we are on the boundaries
                        cell_indices = cell_indices[np.logical_and(cell_indices >= 0,
                                                                   cell_indices < len(neuronal_data))]
                        if sort_raster_by_cell_type:
                            new_neuronal_data[last_index:last_index+len(cell_indices)] = neuronal_data[cell_indices]
                        # coloring the cells
                        cells_to_highlight_colors.extend([cells_group_color]*len(cell_indices))
                        if sort_raster_by_cell_type:
                            cells_to_highlight.extend(list(np.arange(last_index, last_index+len(cell_indices))))
                            last_index = last_index + len(cell_indices)
                            cells_added.extend(list(cell_indices))
                        else:
                            cells_to_highlight.extend(list(cell_indices))

                if sort_raster_by_cell_type:
                    cells_left = np.setdiff1d(np.arange(len(neuronal_data)), cells_added)
                    cells_order = cells_added + list(cells_left)
                    if len(cells_left) > 0:
                        new_neuronal_data[last_index:last_index+len(cells_left)] = neuronal_data[cells_left]
                    neuronal_data = new_neuronal_data

            # to decide if we display the neuronal data as trace or as a binary raster
            # we look at how the data looks like, if there more than 10 different values, then it will be a trace mode
            unique_values = np.unique(neuronal_data)
            # if "trace" in roi_response_serie_info[-1]:
            if len(unique_values) > 10:
                display_traces = True
                display_spike_nums = False
                traces = neuronal_data
                if verbose:
                    print(f"traces shape {traces.shape}")
                spike_nums = None
                # we normalize the traces with z-score
                for trace_index, trace in enumerate(traces):
                    traces[trace_index] = (trace - np.mean(trace)) / np.std(trace)
                # traces = traces[:20]
            else:
                display_traces = False
                display_spike_nums = True
                spike_nums = neuronal_data
                traces = None
                if verbose:
                    print(f"spike_nums shape {spike_nums.shape}")

            if hide_raster_y_ticks_labels:
                y_ticks_labels = None
            else:
                y_ticks_labels = cells_order
            plot_raster(spike_nums=spike_nums,
                        frame_times=frame_times, traces=traces, display_traces=display_traces,
                        bin_size_ms_for_spikes_sum=bin_size_ms_for_spikes_sum,
                        display_dashed_line_with_traces=display_dashed_line_with_traces,
                        traces_lw=traces_lw,
                        file_name="raster_" + session_data.identifier,
                        display_spike_nums=display_spike_nums,
                        with_timestamp_in_file_name=with_timestamps_in_file_name,
                        path_results=self.get_results_path(),
                        save_raster=save_raster,
                        show_raster=False,
                        use_brewer_colors_for_traces=use_brewer_colors_for_traces,
                        dpi=dpi,
                        xkcd_mode=xkcd_mode,
                        show_sum_spikes_as_percentage=True,
                        spike_shape=spike_shape,
                        spike_shape_size=spike_size,
                        without_ticks=not with_ticks,
                        without_activity_sum=not with_activity_sum,
                        cell_spikes_color=spikes_color,
                        figure_background_color=background_color,
                        raster_face_color=background_color,
                        activity_sum_plot_color=activity_sum_plot_color,
                        activity_sum_face_color=background_color,
                        axes_label_color=axes_label_color,
                        hide_x_ticks_labels=hide_x_ticks_labels,
                        hide_raster_y_ticks_labels=hide_raster_y_ticks_labels,
                        raster_y_axis_label=raster_y_axis_label,
                        raster_y_axis_label_size=raster_y_axis_label_size,
                        y_ticks_labels=y_ticks_labels,
                        y_ticks_labels_size=y_axis_ticks_label_size,
                        x_ticks_labels_size=x_axis_ticks_label_size,
                        y_ticks_labels_color=y_ticks_labels_color,
                        x_ticks_labels_color=x_ticks_labels_color,
                        cells_to_highlight=cells_to_highlight,
                        cells_to_highlight_colors=cells_to_highlight_colors,
                        span_area_coords=span_area_coords,
                        span_area_colors=span_area_colors,
                        span_area_only_on_raster=span_area_only_on_raster,
                        alpha_span_area=intervals_alpha_color,
                        size_fig=(width_fig, height_fig),
                        save_formats=save_formats)

            if verbose:
                print(" ")
            self.update_progressbar(time_started=self.analysis_start_time, increment_value=100 / n_sessions)

        print(f"Raster analysis run in {np.round(time() - self.analysis_start_time, 3)} sec")
