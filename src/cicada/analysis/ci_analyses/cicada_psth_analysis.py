from cicada.analysis.cicada_analysis import CicadaAnalysis
from time import time
import numpy as np
from cicada.utils.misc import from_timestamps_to_frame_epochs
from cicada.utils.display.psth import get_psth_values, plot_one_psth, plot_several_psth
import math
from bisect import bisect_right
from cicada.utils.display.colors import BREWER_COLORS
import matplotlib.cm as cm
from cicada.utils.misc import from_timestamps_to_frame_epochs, validate_indices_in_string_format, \
    extract_indices_from_string


class CicadaPsthAnalysis(CicadaAnalysis):
    def __init__(self, config_handler=None):
        """
        A list of
        :param data_to_analyse: list of data_structure
        :param family_id: family_id indicated to which family of analysis this class belongs. If None, then
        the analysis is a family in its own.
        :param data_format: indicate the type of data structure. for NWB, NIX
        """
        CicadaAnalysis.__init__(self, name="PSTH", family_id="Epochs",
                                short_description="Build PeriStimuli Time Histogram", config_handler=config_handler,
                                accepted_data_formats=["CI_DATA"])

        # each key is an int representing the age, the value is a list of session_data
        self.sessions_by_age = dict()

    def copy(self):
        """
        Make a copy of the analysis
        Returns:

        """
        analysis_copy = CicadaPsthAnalysis(config_handler=self.config_handler)
        self.transfer_attributes_to_tabula_rasa_copy(analysis_copy=analysis_copy)
        return analysis_copy

    def check_data(self):
        """
        Check the data given one initiating the class and return True if the data given allows the analysis
        implemented, False otherwise.
        :return: a boolean
        """
        super().check_data()

        for session_index, session_data in enumerate(self._data_to_analyse):
            if session_data.DATA_FORMAT != "CI_DATA":
                self.invalid_data_help = f"Non CI_DATA format compatibility not yet implemented: " \
                                         f"{session_data.DATA_FORMAT}"
                return False

        for data_to_analyse in self._data_to_analyse:
            roi_response_series = data_to_analyse.get_roi_response_series()
            if len(roi_response_series) == 0:
                self.invalid_data_help = f"No roi response series available in " \
                    f"{data_to_analyse.identifier}"
                return False

        # It is also necessary to have epoch to work with, but if None is available, then it won't just be possible
        # to run the analysis

        return True

    def set_arguments_for_gui(self):
        """

        Returns:

        """
        CicadaAnalysis.set_arguments_for_gui(self)

        self.add_roi_response_series_arg_for_gui(short_description="Neural activity to use", long_description=None)

        ages = []
        for session_index, session_data in enumerate(self._data_to_analyse):
            age = session_data.age
            if age not in self.sessions_by_age:
                self.sessions_by_age[age] = []
            self.sessions_by_age[age].append(session_data)
            ages.append(age)
        self.add_choices_for_groups_for_gui(arg_name="sessions_grouped_by_age", choices=np.unique(ages),
                                            with_color=False,
                                            mandatory=False,
                                            short_description="Grouped by age",
                                            long_description="If you select group of ages, PSTH will be produced "
                                                             "averaging PSTH over the session in that age. "
                                                             "You can indicate the "
                                                             "ages in text field, such as '1-4 6 15-17'to "
                                                             "make a group "
                                                             "with age 1 to 4, 6 and 15 to 17. If None are selected, "
                                                             "then a PSTH will be produce for each session.",
                                            family_widget="session_selection",
                                            add_custom_group_field=True,
                                            custom_group_validation_fct=validate_indices_in_string_format,
                                            custom_group_processor_fct=None)

        all_epochs = []
        for data_to_analyse in self._data_to_analyse:
            all_epochs.extend(data_to_analyse.get_intervals_names())
            all_epochs.extend(data_to_analyse.get_behavioral_epochs_names())
        all_epochs = list(np.unique(all_epochs))

        self.add_choices_for_groups_for_gui(arg_name="epochs_names", choices=all_epochs,
                                            with_color=False,
                                            mandatory=False,
                                            short_description="Epochs",
                                            long_description="Select epochs for which you want to build PSTH",
                                            family_widget="epochs")

        self.add_bool_option_for_gui(arg_name="do_fusion_epochs", true_by_default=False,
                                     short_description="Fusion epochs ?",
                                     long_description="If checked, within a group epochs that overlap "
                                                      "will be fused so they represent "
                                                      "then one epoch.",
                                     family_widget="epochs")

        all_cell_types = []
        for data_to_analyse in self._data_to_analyse:
            all_cell_types.extend(data_to_analyse.get_all_cell_types())

        all_cell_types = list(set(all_cell_types))

        self.add_choices_for_groups_for_gui(arg_name="cells_groups", choices=all_cell_types,
                                            with_color=True,
                                            mandatory=False,
                                            short_description="Groups of cells",
                                            long_description="If you select cells's groups, a PSTH will be created "
                                                             "for each cells'group and session. You can indicate the "
                                                             "cell indices in text field, such as '1-4 6 15-17'to "
                                                             "make a group "
                                                             "with cell 1 to 4, 6 and 15 to 17.",
                                            family_widget="cell_type",
                                            add_custom_group_field=True,
                                            custom_group_validation_fct=validate_indices_in_string_format,
                                            custom_group_processor_fct=None)

        self.add_choices_arg_for_gui(arg_name="session_color_choice", choices=["default_color", "brewer", "spectral"],
                                     short_description="Colors for session plots",
                                     default_value="brewer",
                                     multiple_choices=False, long_description=None,
                                     family_widget="session_color_config")

        self.add_color_arg_for_gui(arg_name="session_default_color", default_value=(1, 1, 1, 1.),
                                   short_description="Default color for session",
                                   long_description=None, family_widget="session_color_config")

        self.add_int_values_arg_for_gui(arg_name="psth_range", min_value=50, max_value=10000,
                                        short_description="Range of the PSTH (ms)",
                                        default_value=1500, family_widget="psth_config")

        self.add_int_values_arg_for_gui(arg_name="low_percentile", min_value=1, max_value=49,
                                        short_description="Low percentile",
                                        default_value=25, family_widget="psth_config")

        self.add_int_values_arg_for_gui(arg_name="high_percentile", min_value=51, max_value=99,
                                        short_description="High percentile",
                                        default_value=75, family_widget="psth_config")

        self.add_choices_arg_for_gui(arg_name="ref_in_epoch", choices=["start", "end", "middle"],
                                     short_description="Reference point to center PSTH",
                                     long_description="Determine which part of the epoch to center the PSTH on",
                                     default_value="start",
                                     multiple_choices=False, family_widget="psth_config")

        self.add_choices_arg_for_gui(arg_name="plot_style_option", choices=["lines", "bars"],
                                     short_description="Options to display the PSTH",
                                     default_value="lines",
                                     multiple_choices=False, long_description=None, family_widget="plot_config")

        self.add_bool_option_for_gui(arg_name="plots_with_same_y_scale", true_by_default=True,
                                     short_description="Same scale for Y axis",
                                     long_description=None,
                                     family_widget="plot_config")

        self.add_bool_option_for_gui(arg_name="average_fig", true_by_default=False,
                                     short_description="Add a figure that average all sessions",
                                     long_description=None,
                                     family_widget="plot_config")

        self.add_bool_option_for_gui(arg_name="one_fig_by_group_and_epoch", true_by_default=False,
                                     short_description="Plot a single figure for each group and each type of epoch",
                                     long_description=None,
                                     family_widget="plot_config")

        self.add_choices_arg_for_gui(arg_name="x_axis_scale", choices=["ms", "sec", "min"],
                                     short_description="X-axis scale",
                                     default_value="sec",
                                     multiple_choices=False, long_description=None, family_widget="plot_config")

        self.add_bool_option_for_gui(arg_name="with_surrogates", true_by_default=False,
                                     short_description="Use surrogate to define a threshold of activation",
                                     long_description=None,
                                     family_widget="surrogates")

        self.add_int_values_arg_for_gui(arg_name="n_surrogates", min_value=10, max_value=10000,
                                        short_description="Number of surrogates",
                                        default_value=500, family_widget="surrogates")

        self.add_int_values_arg_for_gui(arg_name="surrogate_percentile", min_value=90, max_value=99,
                                        short_description="Surrogates percentile threshold",
                                        default_value=95, family_widget="surrogates")

        # TODO: fixe the x-axis ticks

        self.add_image_format_package_for_gui()

        self.add_verbose_arg_for_gui()

        self.add_with_timestamp_in_filename_arg_for_gui()

    def update_original_data(self):
        """
        To be called if the data to analyse should be updated after the analysis has been run.
        :return: boolean: return True if the data has been modified
        """
        pass

    def run_analysis(self, **kwargs):
        """
        test
        :param kwargs:
        :return:
        """
        CicadaAnalysis.run_analysis(self, **kwargs)

        roi_response_series_dict = kwargs["roi_response_series"]

        epochs_names = kwargs.get("epochs_names")
        if (epochs_names is None) or len(epochs_names) == 0:
            print(f"No epochs selected, no analysis")
            self.update_progressbar(time_started=self.analysis_start_time, increment_value=100)
            return

        cells_groups_dict = kwargs.get("cells_groups")
        # used to know if there are or not cell type groups
        no_cell_type_groups_tag = "_no_cell_type_groups_"

        # allows to compute a significant threshold
        with_surrogates = kwargs.get("with_surrogates", False)
        n_surrogates = kwargs.get("n_surrogates", 500)
        surrogate_percentile = kwargs.get("surrogate_percentile", 95)

        # ------- figures config part -------
        save_formats = kwargs["save_formats"]
        if save_formats is None:
            save_formats = "pdf"

        dpi = kwargs.get("dpi", 100)

        width_fig = kwargs.get("width_fig")

        height_fig = kwargs.get("height_fig")

        ref_in_epoch = kwargs.get("ref_in_epoch")

        psth_range_in_ms = kwargs.get("psth_range")
        psth_range_in_sec = psth_range_in_ms / 1000

        # if True then the plots should have the same Y axis scale
        plots_with_same_y_scale = kwargs.get("plots_with_same_y_scale", True)

        with_timestamps_in_file_name = kwargs.get("with_timestamp_in_file_name", True)

        session_color_choice = kwargs.get("session_color_choice")

        session_default_color = kwargs.get("session_default_color")

        one_fig_by_group_and_epoch = kwargs.get("one_fig_by_group_and_epoch")

        x_axis_scale = kwargs.get("x_axis_scale", "sec")

        # TODO: Add option to plot a "significant threshold" line using surrogates
        #  Would mean take each neuronal_data roll them x times, keep the max value of each surrogate
        #  an take the 95th percentile at each frames to determine thresholds ?
        #

        # ------- figures config part -------

        # we can either run the PSTH on all data from all session
        # or do a PSTH for each session individually and produce as many plots
        # or add option to group sessions for example by age

        # dict results from widget
        groups_by_age_from_widget = kwargs.get("sessions_grouped_by_age")

        # first we create a dict that will identify group of session by an id
        # a group could be just a session
        # dict key is a string, value is a list of CicadaAnalysisWrapper instances
        session_group_ids_dict = dict()

        # take as key a session identifier and return the group_id it is part of
        session_to_group_id_mapping = dict()

        # then we have a dict with key epoch_name, value is a dict with key session_group_id and value a list of 2
        # elements: the time (in sec) of each value time of the PSTH, and PSTH data: list of length the number of fct
        # used each list being a list of float of length the number of timestamps
        psth_data_by_epoch_dict = dict()

        # same structure than psth_data_by_epoch_dict but at the end instead of the data, there is a dict
        # with keys the count for the different legends such as
        # number of subjects, number of sessions, number of cells, number of epochs
        psth_legends_by_epoch_dict = dict()

        # same than 2 others but the value is a list of all max_values of each neuronal_data psth median values
        # then percentile can be applied
        psth_surrogates_by_epoch_dict = dict()

        # then we have a dict with key session_group_id, value is a dict with key epoch_name and value a list of 2
        # elements: the time (in sec) of each value time of the PSTH, and PSTH data: list of length the number of fct
        # used each list being a list of float of length the number of timestamps
        psth_data_by_session_group_dict = dict()

        # same structure than psth_data_by_session_group_dict but at the end instead of the data, there is a dict
        # with keys the count for the different legends such as
        # number of subjects, number of sessions, number of cells, number of epochs
        psth_legends_by_session_group_dict = dict()

        # same than 2 others but the value is a list of all max_values of each neuronal_data psth median values
        # then percentile can be applied
        psth_surrogates_by_session_group_dict = dict()

        epochs_color_dict = dict()
        for epoch_group_name, epoch_info in epochs_names.items():
            if len(epoch_info) != 2:
                continue
            epochs_color_dict[epoch_group_name] = epoch_info[1]

        n_sessions = len(self._data_to_analyse)

        low_percentile = kwargs.get("low_percentile", 25)
        high_percentile = kwargs.get("high_percentile", 75)

        fcts_to_apply = [np.nanmedian, lambda x: np.nanpercentile(x, low_percentile),
                         lambda x: np.nanpercentile(x, high_percentile)]

        # first building the groups
        if len(groups_by_age_from_widget) > 0:
            # print(f"groups_by_age_from_widget {groups_by_age_from_widget}")
            # return
            for session_group_id, session_group_info in groups_by_age_from_widget.items():
                # session_group_info is a list with 2 elements, the second one is a color code
                # (4 float between 0 and 1) or None if not color option is activated.
                # For exemple: {'5-6': [['5', '6'], (1.0, 1.0, 1.0, 1.0)], '7-10': [['7-10'], (1.0, 1.0, 1.0, 1.0)]}
                # the first element is either a list of string representing int, or a list of more complex int like
                # 7-10 meaning from p7 to p10
                ages_in_group = []
                age_codes_in_group = session_group_info[0]
                group_color = session_group_info[1]
                for age_code in age_codes_in_group:
                    try:
                        age = int(age_code)
                        ages_in_group.append(age)
                    except ValueError:
                        ages_in_group.extend(extract_indices_from_string(age_code))

                session_group_ids_dict[session_group_id] = []
                for age in ages_in_group:
                    if age in self.sessions_by_age:
                        sessions = self.sessions_by_age[age]
                        for session_data in sessions:
                            session_identifier = session_data.identifier
                            session_to_group_id_mapping[session_identifier] = session_group_id
                            session_group_ids_dict[session_group_id].append(session_data)

        else:
            # each session is its own group
            for session_index, session_data in enumerate(self._data_to_analyse):
                session_identifier = session_data.identifier
                session_to_group_id_mapping[session_identifier] = session_identifier
                session_group_ids_dict[session_identifier] = [session_data]

        highest_sampling_rate = 0
        lowest_sampling_rate = 100000
        for session_index, session_data in enumerate(self._data_to_analyse):
            session_identifier = session_data.identifier
            if session_identifier not in session_to_group_id_mapping:
                # if the session does not belong to any group, we skip it
                continue
            print(f"-------------- {session_identifier} -------------- ")

            sampling_rate_hz = session_data.get_ci_movie_sampling_rate(only_2_photons=True)
            if sampling_rate_hz > highest_sampling_rate:
                highest_sampling_rate = sampling_rate_hz
            if sampling_rate_hz < lowest_sampling_rate:
                lowest_sampling_rate = sampling_rate_hz

            one_frame_duration = 1000 / sampling_rate_hz
            psth_range_in_frames = math.ceil(psth_range_in_ms / one_frame_duration)

            if isinstance(roi_response_series_dict, dict):
                roi_response_serie_info = roi_response_series_dict[session_identifier]
            else:
                roi_response_serie_info = roi_response_series_dict

            neuronal_data = session_data.get_roi_response_serie_data(keys=roi_response_serie_info)

            neuronal_data_timestamps = session_data.get_roi_response_serie_timestamps(keys=roi_response_serie_info)

            for epoch_group_name, epoch_info in epochs_names.items():
                if len(epoch_info) != 2:
                    continue
                epochs_names_in_group = epoch_info[0]
                # epoch_color = epoch_info[1]

                # TODO: take into consideration invalid epochs, and remove the one in invalid section
                epochs_frames_in_group = []
                invalid_times = session_data.get_interval_times(interval_name="invalid_times")
                extended_invalid_times = None
                invalid_times_are_sorted = False

                # first we extent the invalid times to take into consideration the range of PSTH
                if invalid_times is not None:
                    invalid_times_are_sorted = np.all(np.diff(invalid_times[1]) >= 0)
                    extended_invalid_times = np.zeros(invalid_times.shape)
                    for index in range(invalid_times.shape[1]):
                        start_ts = invalid_times[0, index]
                        stop_ts = invalid_times[1, index]
                        start_ts = max(0, start_ts - psth_range_in_sec)
                        stop_ts = stop_ts + psth_range_in_sec
                        extended_invalid_times[0, index] = start_ts
                        extended_invalid_times[1, index] = stop_ts

                # print(f"session_data.get_intervals_names() {session_data.get_intervals_names()}")
                if extended_invalid_times is not None:
                    print(f"Using invalid times, among {epoch_group_name}, we remove:")
                for epoch_name in epochs_names_in_group:
                    # looking in behavior or intervals
                    epochs_timestamps = session_data.get_interval_times(interval_name=epoch_name)
                    if epochs_timestamps is None:
                        epochs_timestamps = session_data.get_behavioral_epochs_times(epoch_name=epoch_name)
                    if epochs_timestamps is None:
                        # means this session doesn't have this epoch name
                        continue
                    epochs_are_sorted = np.all(np.diff(epochs_timestamps[1]) >= 0)
                    if extended_invalid_times is not None:
                        # now we want to filter epochs_timestamps
                        # we loop over each invalid epoch to remove the epoch that overlap it
                        # TODO: not super efficient, see to make it more efficient
                        filtered_epochs_timestamps = np.zeros(epochs_timestamps.shape)
                        n_epochs_kept = 0
                        for epoch_index in range(epochs_timestamps.shape[1]):
                            epoch_start_ts = epochs_timestamps[0, epoch_index]
                            epoch_stop_ts = epochs_timestamps[1, epoch_index]
                            # if ordered, and the epoch if superior at the last invalid frames known
                            # we can skip it
                            if invalid_times_are_sorted and epochs_are_sorted and \
                                    (epoch_start_ts > extended_invalid_times[1, -1]):
                                filtered_epochs_timestamps[:, n_epochs_kept] = epochs_timestamps[:, epoch_index]
                                n_epochs_kept += 1
                                break
                            to_filter = False
                            for invalid_index in range(extended_invalid_times.shape[1]):
                                invalid_start_ts = extended_invalid_times[0, invalid_index]
                                invalid_stop_ts = extended_invalid_times[1, invalid_index]

                                if (epoch_start_ts >= invalid_start_ts) and (epoch_start_ts <= invalid_stop_ts):
                                    to_filter = True
                                    break
                                if (epoch_stop_ts >= invalid_start_ts) and (epoch_stop_ts <= invalid_stop_ts):
                                    to_filter = True
                                    break
                                if (epoch_start_ts <= invalid_start_ts) and (epoch_stop_ts >= invalid_stop_ts):
                                    to_filter = True
                                    break

                            if not to_filter:
                                filtered_epochs_timestamps[:, n_epochs_kept] = epochs_timestamps[:, epoch_index]
                                n_epochs_kept += 1

                        filtered_epochs_timestamps = filtered_epochs_timestamps[:, :n_epochs_kept]
                        n_epochs_filtered = epochs_timestamps.shape[1] - filtered_epochs_timestamps.shape[1]
                        print(f"{n_epochs_filtered} {epoch_name} epochs")
                        epochs_timestamps = filtered_epochs_timestamps

                    # session_data.get_interval_times()
                    # now we want to get the intervals time_stamps and convert them in frames
                    # list of list of 2 int
                    intervals_frames = from_timestamps_to_frame_epochs(time_stamps_array=epochs_timestamps,
                                                                       frames_timestamps=neuronal_data_timestamps,
                                                                       as_list=True)

                    epochs_frames_in_group.extend(intervals_frames)
                if extended_invalid_times is not None:
                    print(f" ")

                session_group_id = session_to_group_id_mapping[session_identifier]
                # then we have a dict with key epoch_name, value is a dict with key
                # session_group_id and value the PSTH data
                if epoch_group_name not in psth_data_by_epoch_dict:
                    psth_data_by_epoch_dict[epoch_group_name] = dict()
                    psth_legends_by_epoch_dict[epoch_group_name] = dict()
                    psth_surrogates_by_epoch_dict[epoch_group_name] = dict()

                # if no cells group selected, then we use all cells
                if len(cells_groups_dict) == 0:
                    cells_groups_dict[no_cell_type_groups_tag] = None

                cell_indices_by_cell_type = session_data.get_cell_indices_by_cell_type(roi_serie_keys=
                                                                                       roi_response_serie_info)

                for cells_group_name, cells_group_info in cells_groups_dict.items():
                    if len(cells_group_info) != 2:
                        continue
                    cells_names_in_group = cells_group_info[0]
                    cells_group_color = cells_group_info[1]
                    cell_indices_in_group = []

                    if cells_group_name not in psth_data_by_epoch_dict[epoch_group_name]:
                        psth_data_by_epoch_dict[epoch_group_name][cells_group_name] = dict()
                        psth_legends_by_epoch_dict[epoch_group_name][cells_group_name] = dict()
                        psth_surrogates_by_epoch_dict[epoch_group_name][cells_group_name] = dict()
                    if cells_group_name not in psth_data_by_session_group_dict:
                        psth_data_by_session_group_dict[cells_group_name] = dict()
                        psth_legends_by_session_group_dict[cells_group_name] = dict()
                        psth_surrogates_by_session_group_dict[cells_group_name] = dict()

                    if cells_names_in_group == no_cell_type_groups_tag:
                        cell_indices_in_group = np.arange(len(neuronal_data))
                    else:
                        for cells_name in cells_names_in_group:
                            # then we extract the cell_indices from the cells_group_name
                            # it is either a string representing the cell_type or a series of indices
                            if cells_name in cell_indices_by_cell_type:
                                cell_indices = cell_indices_by_cell_type[cells_name]
                            else:
                                cell_indices = np.array(extract_indices_from_string(cells_name))
                            if len(cell_indices) == 0:
                                continue
                            # making sure we are on the boundaries
                            cell_indices_in_group.extend(list(cell_indices[np.logical_and(cell_indices >= 0,
                                                              cell_indices < len(neuronal_data))]))
                    cell_indices_in_group = np.array(cell_indices_in_group)

                    # TODO: See for fusionning epochs from a same group so there are extended
                    # psth_values is a list of the same length as fct_to_apply,
                    # containing a list of float of size '(range_value*2)+1'
                    psth_frames_indices, psth_values = get_psth_values(data=neuronal_data[cell_indices_in_group],
                                                                       epochs=epochs_frames_in_group,
                                                                       ref_in_epoch="start",
                                                                       range_value=psth_range_in_frames,
                                                                       fcts_to_apply=fcts_to_apply)
                    if with_surrogates:
                        surrogate_max_values = []
                        for index_surrogate in range(n_surrogates):
                            surrogate_neuronal_data = np.copy(neuronal_data[cell_indices_in_group])
                            for cell in np.arange(len(surrogate_neuronal_data)):
                                surrogate_neuronal_data[cell, :] = np.roll(surrogate_neuronal_data[cell, :],
                                                                           np.random.randint(1, surrogate_neuronal_data.shape[1]))
                            surro_psth_frames_indices, surro_psth_values = get_psth_values(
                                data=surrogate_neuronal_data,
                                epochs=epochs_frames_in_group,
                                ref_in_epoch="start",
                                range_value=psth_range_in_frames,
                                fcts_to_apply=fcts_to_apply)
                            # psth_frames_indices, psth_values = surro_psth_frames_indices, surro_psth_values
                            # we take the max of the median
                            # max_psth_value = np.max(surro_psth_values[0])
                            # surrogate_max_values[index_surrogate] = max_psth_value
                            # print(f"surro_psth_values[0] {surro_psth_values[0]}")
                            # surrogate_max_values.extend(list(surro_psth_values[0]))
                            surrogate_max_values.extend([np.max(surro_psth_values[0])])
                        if session_group_id not in psth_data_by_epoch_dict[epoch_group_name][cells_group_name]:
                            psth_surrogates_by_epoch_dict[epoch_group_name][cells_group_name][session_group_id] = []
                        psth_surrogates_by_epoch_dict[epoch_group_name][cells_group_name][session_group_id].extend(
                            surrogate_max_values)

                        if session_group_id not in psth_surrogates_by_session_group_dict[cells_group_name]:
                            psth_surrogates_by_session_group_dict[cells_group_name][session_group_id] = dict()
                        if epoch_group_name not in psth_surrogates_by_session_group_dict[cells_group_name][session_group_id]:
                            psth_surrogates_by_session_group_dict[cells_group_name][session_group_id][epoch_group_name] = []
                        psth_surrogates_by_session_group_dict[cells_group_name][session_group_id][epoch_group_name].extend(surrogate_max_values)

                    # TODO: See to add rolling here ?
                    # then take np.max(psth_values)
                    # putting indices in msec
                    psth_frames_indices = np.array(psth_frames_indices)
                    psth_times = psth_frames_indices * 1000 / sampling_rate_hz
                    if session_group_id not in psth_data_by_epoch_dict[epoch_group_name][cells_group_name]:
                        psth_data_by_epoch_dict[epoch_group_name][cells_group_name][session_group_id] = []
                        psth_legends_by_epoch_dict[epoch_group_name][cells_group_name][session_group_id] = dict()
                        legends_dict = psth_legends_by_epoch_dict[epoch_group_name][cells_group_name][session_group_id]
                        legends_dict["subjects"] = []
                        legends_dict["n_sessions"] = 0
                        legends_dict["n_cells"] = 0
                        legends_dict["n_epochs"] = 0

                    psth_data_by_epoch_dict[epoch_group_name][cells_group_name][session_group_id].append(
                        [psth_times, psth_values])
                    legends_dict = psth_legends_by_epoch_dict[epoch_group_name][cells_group_name][session_group_id]
                    legends_dict["subjects"].append(session_data.subject_id)
                    legends_dict["n_sessions"] += 1
                    legends_dict["n_cells"] += len(cell_indices_in_group)
                    legends_dict["n_epochs"] += len(epochs_frames_in_group)

                    # then we have a dict with key session_group_id, value is a dict with key epoch_name
                    # and value the PSTH data
                    if session_group_id not in psth_data_by_session_group_dict[cells_group_name]:
                        psth_data_by_session_group_dict[cells_group_name][session_group_id] = dict()
                        psth_legends_by_session_group_dict[cells_group_name][session_group_id] = dict()

                    if epoch_group_name not in psth_data_by_session_group_dict[cells_group_name][session_group_id]:
                        psth_data_by_session_group_dict[cells_group_name][session_group_id][epoch_group_name] = []
                        # number of subjects, number of sessions, number of cells, number of epochs
                        psth_legends_by_session_group_dict[cells_group_name][session_group_id][epoch_group_name] = dict()
                        legends_dict = psth_legends_by_session_group_dict[cells_group_name][session_group_id][epoch_group_name]
                        legends_dict["subjects"] = []
                        legends_dict["n_sessions"] = 0
                        legends_dict["n_cells"] = 0
                        legends_dict["n_epochs"] = 0

                    psth_data_by_session_group_dict[cells_group_name][session_group_id][epoch_group_name].append(
                        [psth_times, psth_values])

                    legends_dict = psth_legends_by_session_group_dict[cells_group_name][session_group_id][
                        epoch_group_name]
                    legends_dict["subjects"].append(session_data.subject_id)
                    legends_dict["n_sessions"] += 1
                    legends_dict["n_cells"] += len(cell_indices_in_group)
                    legends_dict["n_epochs"] += len(epochs_frames_in_group)

            self.update_progressbar(time_started=self.analysis_start_time, increment_value=100 / (n_sessions+1))

        # first we determine the bins to use
        # using the lowest sampling_rate to get the biggest bins
        step_in_ms = 1000 / lowest_sampling_rate
        bins_edges = np.arange(-psth_range_in_ms, psth_range_in_ms + step_in_ms, step_in_ms)
        bins_edges[-1] = psth_range_in_ms
        # print(f"bins_edges {bins_edges}")
        # print(f"len(bins_edges) {len(bins_edges)}, len(psth_times) {len(psth_times)}")
        x_label = f"Duration ({x_axis_scale})"
        y_label = "Activity (%)"
        # now we plot the PSTH
        # we want to make a figure for each session group with all epoch types in it
        for cells_group_name in psth_data_by_session_group_dict.keys():
            for session_group_id, epoch_dict in psth_data_by_session_group_dict[cells_group_name].items():
                data_psth = []
                threshold_values = [] if with_surrogates else None
                colors_plot = []
                label_legends = []
                cells_groups_descr = ""
                if cells_group_name != no_cell_type_groups_tag:
                    cells_groups_descr = f"_{cells_group_name}"
                for epoch_group_name, epoch_data_list in epoch_dict.items():
                    # epoch_data_list contains data for each session
                    time_x_values, psth_values_for_plot = \
                        prepare_psth_values(epoch_data_list, bins_edges, low_percentile, high_percentile)
                    if x_axis_scale == "sec":
                        time_x_values = [t / 1000 for t in time_x_values]

                    elif x_axis_scale == "min":
                        time_x_values = [t / 60000 for t in time_x_values]

                    data_psth.append([time_x_values, psth_values_for_plot])
                    colors_plot.append(epochs_color_dict[epoch_group_name])

                    if with_surrogates:
                        surrogate_max_values = psth_surrogates_by_session_group_dict[cells_group_name][session_group_id][
                        epoch_group_name]
                        # print(f"np.percentile(surrogate_max_values, surrogate_percentile) {np.percentile(surrogate_max_values, surrogate_percentile)}")
                        # put them in percentage
                        threshold_values.append(np.percentile(surrogate_max_values, surrogate_percentile) * 100)

                    legends_dict = psth_legends_by_session_group_dict[cells_group_name][session_group_id][
                        epoch_group_name]

                    n_subjects = len(set(legends_dict["subjects"]))
                    n_sessions = legends_dict["n_sessions"]
                    n_cells = legends_dict["n_cells"]
                    n_epochs = legends_dict["n_epochs"]
                    label_legends.append(f"{n_epochs} {epoch_group_name}" + "\n" +
                                         f"N={n_subjects}, n={n_sessions}" + "\n" + f"{n_cells} cells")

                plot_several_psth(results_path=self.get_results_path(),
                                  data_psth=data_psth,
                                  colors_plot=colors_plot,
                                  file_name=f"psth_{session_group_id}{cells_groups_descr}",
                                  label_legends=label_legends,
                                  x_label=x_label, y_label=y_label,
                                  color_ticks="white",
                                  axes_label_color="white",
                                  threshold_line_y_values=threshold_values,
                                  with_same_y_scale=plots_with_same_y_scale,
                                  color_v_line="white", line_width=2,
                                  line_mode=True, background_color="black",
                                  figsize=(30, 20),
                                  save_formats=save_formats,
                                  summary_plot=True)

        # we want to make a figure for each epoch types and cell_typewith all the session group
        for epoch_group_name, session_dict in psth_data_by_epoch_dict.items():
            session_index = 0
            n_sessions = len(session_dict)
            for cells_group_name in session_dict.keys():
                data_psth = []
                # same length as data_psth, display an horizontal line representing a statistical threshold
                threshold_values = [] if with_surrogates else None
                colors_plot = []
                label_legends = []
                cells_groups_descr = ""
                if cells_group_name != no_cell_type_groups_tag:
                    cells_groups_descr = f"_{cells_group_name}"

                for session_group_id, session_data_list in session_dict[cells_group_name].items():
                    time_x_values, psth_values_for_plot = \
                        prepare_psth_values(session_data_list, bins_edges, low_percentile, high_percentile)

                    if x_axis_scale == "sec":
                        time_x_values = [t / 1000 for t in time_x_values]

                    elif x_axis_scale == "min":
                        time_x_values = [t / 60000 for t in time_x_values]

                    data_psth.append([time_x_values, psth_values_for_plot])
                    # "default_color", "brewer", "spectral"
                    if session_color_choice == "brewer":
                        colors_plot.append(BREWER_COLORS[session_index % len(BREWER_COLORS)])
                    elif session_color_choice == "spectral":
                        colors_plot.append(cm.nipy_spectral(float(session_index + 1) / (n_sessions + 1)))
                    else:
                        colors_plot.append(session_default_color)

                    legends_dict = psth_legends_by_epoch_dict[epoch_group_name][cells_group_name][session_group_id]

                    if with_surrogates:
                        surrogate_max_values = psth_surrogates_by_epoch_dict[epoch_group_name][cells_group_name][session_group_id]
                        # print(f"np.percentile(surrogate_max_values, surrogate_percentile) {np.percentile(surrogate_max_values, surrogate_percentile)}")
                        # put them in percentage
                        threshold_values.append(np.percentile(surrogate_max_values, surrogate_percentile)*100)

                    n_subjects = len(set(legends_dict["subjects"]))
                    n_sessions = legends_dict["n_sessions"]
                    n_cells = legends_dict["n_cells"]
                    n_epochs = legends_dict["n_epochs"]
                    label_legends.append(f"{session_group_id}" + "\n" +
                                         f"{n_epochs} {epoch_group_name}" + "\n" +
                                         f"N={n_subjects}, n={n_sessions}" + "\n" + f"{n_cells} cells")
                    session_index += 1
                plot_several_psth(results_path=self.get_results_path(),
                                  data_psth=data_psth,
                                  colors_plot=colors_plot,
                                  file_name=f"psth_{epoch_group_name}{cells_groups_descr}",
                                  label_legends=label_legends,
                                  x_label=x_label, y_label=y_label,
                                  color_ticks="white",
                                  axes_label_color="white",
                                  color_v_line="white", line_width=2,
                                  line_mode=True, background_color="black",
                                  threshold_line_y_values=threshold_values,
                                  with_same_y_scale=plots_with_same_y_scale,
                                  figsize=(30, 20),
                                  save_formats=save_formats,
                                  summary_plot=True)

        # we want a figure for each epoch session group and each epoch
        if one_fig_by_group_and_epoch:
            for epoch_group_name, session_dict in psth_data_by_epoch_dict.items():
                session_index = 0
                n_sessions = len(session_dict)
                for cells_group_name in session_dict.keys():
                    cells_groups_descr = ""
                    if cells_group_name != no_cell_type_groups_tag:
                        cells_groups_descr = f"_{cells_group_name}"
                    for session_group_id, session_data_list in session_dict[cells_group_name].items():
                        time_x_values, psth_values_for_plot = \
                            prepare_psth_values(session_data_list, bins_edges, low_percentile, high_percentile)
                        if session_color_choice == "brewer":
                            color_plot = BREWER_COLORS[session_index % len(BREWER_COLORS)]
                        elif session_color_choice == "spectral":
                            color_plot = cm.nipy_spectral(float(session_index + 1) / (n_sessions + 1))
                        else:
                            color_plot = session_default_color

                        legends_dict = psth_legends_by_epoch_dict[epoch_group_name][cells_group_name][session_group_id]

                        n_subjects = len(set(legends_dict["subjects"]))
                        n_sessions = legends_dict["n_sessions"]
                        n_cells = legends_dict["n_cells"]
                        n_epochs = legends_dict["n_epochs"]
                        label_legends.append(f"{session_group_id}" + "\n" +
                                             f"{n_epochs} {epoch_group_name}" + "\n" +
                                             f"N={n_subjects}, n={n_sessions}" + "\n" + f"{n_cells} cells")
                        session_index += 1
                        plot_one_psth(results_path=self.get_results_path(),
                                      time_x_values=time_x_values, psth_values=psth_values_for_plot,
                                      color_plot=color_plot,
                                      x_label=x_label, y_label=y_label,
                                      color_ticks="white",
                                      axes_label_color="white",
                                      color_v_line="white", label_legend=None, line_width=2,
                                      line_mode=True, background_color="black", ax_to_use=None,
                                      figsize=(15, 10),
                                      file_name=f"psth_{epoch_group_name}_{session_group_id}{cells_groups_descr}",
                                      save_formats=save_formats,
                                      put_mean_line_on_plt=False)

        self.update_progressbar(time_started=self.analysis_start_time, increment_value=100 / (n_sessions + 1))
        print(f"PSTH analysis run in {time() - self.analysis_start_time} sec")


def prepare_psth_values(session_data_list, bins_edges, low_percentile, high_percentile):
    if len(session_data_list) == 1:
        session_data = session_data_list[0]
        psth_times, psth_values = session_data
        median_psth_values = fill_bins(bin_edges=bins_edges, data=psth_values[0],
                                       data_time_points=psth_times)
        low_threshold_psth_values = fill_bins(bin_edges=bins_edges, data=psth_values[1],
                                              data_time_points=psth_times)
        high_threshold_psth_values = fill_bins(bin_edges=bins_edges, data=psth_values[2],
                                               data_time_points=psth_times)
    else:
        psth_matrix = np.zeros((0, len(bins_edges) - 1))
        for session_data in session_data_list:
            psth_times, psth_values = session_data
            # print(f"psth_times {psth_times}")
            psth_values_norm = fill_bins(bin_edges=bins_edges, data=psth_values[0],
                                         data_time_points=psth_times)
            psth_values_norm = np.reshape(psth_values_norm, (1, len(psth_values_norm)))
            psth_matrix = np.concatenate((psth_matrix, psth_values_norm))
        median_psth_values = np.nanmedian(psth_matrix, axis=0)
        low_threshold_psth_values = np.nanpercentile(psth_matrix, low_percentile, axis=0)
        high_threshold_psth_values = np.nanpercentile(psth_matrix, high_percentile, axis=0)
    psth_values_for_plot = [median_psth_values * 100, low_threshold_psth_values * 100,
                            high_threshold_psth_values * 100]
    time_x_values = [(t + bins_edges[i + 1]) / 2 for i, t in enumerate(bins_edges[:-1])]

    return time_x_values, psth_values_for_plot


def fill_bins(bin_edges, data, data_time_points):
    """
    Create an array based on bin_edges. If two values are on the same edge, then we put the mean
    Args:
        bin_edges: 1d array representing the edges of the bins
        data: 1d array representing the value to fill in the bins
        data_time_points: time indices of the data values in order to fit them in the good bin

    Returns: a 1d array of length len(bin_edges) -1

    """
    result = np.zeros(len(bin_edges)-1)

    for value_index, data_value in enumerate(data):
        time_point = data_time_points[value_index]
        # now we want to know in which bin to insert it
        index = bisect_right(bin_edges, time_point) - 1
        if index >= len(result):
            index = len(result) - 1
        # TODO: plan the case when more than 2 values could be on the same bin
        if result[index] > 0:
            # should not happen often
            result[index] = np.mean((result[index], data_value))
        else:
            result[index] = data_value

    return result
